# reivilibre-font-unfading

This font (with TTF, WOFF and WOFF2 versions) is a conversion of the *Amaranth* font by Gesine Todt.

The original was acquired from this website: https://fontlibrary.org/ .

## Licence

As before, this font is licensed under the Open Font License [sic] and this is a derivative work.

Read OFL.txt for precise details.
